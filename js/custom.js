(function($) {

	/**
	The WordPress engine has not made it easy to add a class to the comment form itself, 
	nor the title introducing the comment form.  
	I have designed the site to expand and collapse certain sections depending on the
	presence of certain class conventions.
		(ie, clicking on a element with the '[classname]__reveal' pattern will toggle the 
		visibilty of the corresponding '[classname]__hidden' element)
	Thus, while not the fastest solution, adding classes via jquery as the page is loading
	is one of the easiest.
	*/
	function setup_collapsable_form(classname)
	{
		var $respond = $('div#respond');

		$respond.find("h3#reply-title").addClass(classname + "__reveal");
		$respond.find("form#commentform").addClass(classname + "__hidden");
	}

	/**
	function that hides all elements with a classname containing the word "hidden"
	*/
	function hide_on_load()
	{
		$('*[class*=hidden]').hide();
	}

	/**
	function that, given an array of classes, looks for the class ending in "__reveal"
	The assumption is that for every element with a class following the "[classname]__reveal" 
	convention, there is a corresponding element with the class "[classname]__hidden"

	Thus, once this method has isolated the (first) classname it finds ending in "__reveal", it 
	returns the classname as a string, but with "__hidden" at the end rather than "__reveal"
	*/
	function return_hidden_classname(class_array)
	{
		for (var i = 0; i < class_array.length; i++) {

			var if_class_contains_reveal = class_array[i].indexOf("__reveal");

			if (if_class_contains_reveal >= 0)
			{
				var reveal_class = class_array[i].slice(0, if_class_contains_reveal);

				return reveal_class + "__hidden";
			}

		  }

		return false;
	}
	

	$( document ).ready(function() {

		/*setup_collapsable_form("form_title");*/

  		hide_on_load();

  		var toggle_speed = 300;

  		/**
  		if an element with a classname ending in "__reveal" is clicked,
  		a corresponding element ending in "__hidden" will be toggled
  		*/
  		$('*[class*=reveal]').on("click", function() {

			var class_array = $(this).attr("class").split(" ");

			var class_hidden = return_hidden_classname(class_array);
			//get the classlist of the element in question.

			if(class_hidden != false)
			{
				$("." + class_hidden).slideToggle(toggle_speed);
			}

		});

	});

})(jQuery);



