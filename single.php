<?php
/**
 * The Template for displaying all single posts.
 *
 * @package untitled-child
 *
 **********
 * EDITS:
 * 		-Removed the minislider displaying a carousel of the posts.
 *			Slider can be seen here: http://untitleddemo.wordpress.com/2013/03/07/pellentesque-habitant-posuere/
 **********
 */

get_header();  ?>

	<?php if ( '' != get_the_post_thumbnail() ) { ?>
	<div class="singleimg"><?php the_post_thumbnail( 'slider-img' ); ?></div>

	<?php } ?>

	<div id="single-main" class="site-main">

		<div id="single-primary" class="content-area">
			<div id="content" class="site-content" role="main">

			<?php while ( have_posts() ) : the_post(); ?>

				<?php get_template_part( 'content', 'single' ); ?>

				<?php untitled_content_nav( 'nav-below' ); ?>

				<?php
					// If comments are open or we have at least one comment, load up the comment template
					if ( comments_open() || '0' != get_comments_number() )
						comments_template( '', true );
				?>

			<?php endwhile; // end of the loop. ?>

			</div><!-- #content .site-content -->
		</div><!-- #primary .content-area -->

<?php get_sidebar(); ?>
<?php get_footer(); ?>