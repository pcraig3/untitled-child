<?php
/**
 * untitled-child additional method including a custom javascript asset.
 *
 * @package untitled-child
 */

/**
 * Enqueue scripts and styles
 */
function untitled_child_scripts() {

	wp_enqueue_script( 'custom', get_stylesheet_directory_uri() . '/js/custom.js', array( 'jquery' ), '20120206', true );

}
add_action( 'wp_enqueue_scripts', 'untitled_child_scripts' ); 

//echo get_stylesheet_directory_uri(); 
?>